import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | berichten-centrum/berichten', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:berichten-centrum/berichten');
    assert.ok(controller);
  });
});
