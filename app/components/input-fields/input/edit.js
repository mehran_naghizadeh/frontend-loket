import InputFieldComponent from '@lblod/ember-mu-dynamic-forms/components/input-fields/input/edit';

export default InputFieldComponent.extend({
  classNames: 'u-spacer--bottom--small'
});
