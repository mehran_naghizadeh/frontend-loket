import DS from 'ember-data';
const { attr } = DS;
import { computed } from '@ember/object';

export default DS.Model.extend({

    typeCommunicatie:       attr('string'),
    dossiernummer:          attr('string'),
    reactietermijn:         attr('date'),
    laatsteBericht:         attr('string'),
    laatstVerstuurdDoor:    attr('string'),
    messageBody:            attr('string'),
    aangekomenDatum:        attr('date'),
    verzondenDatum:         attr('date'),
    title:                  attr('string'),

    niceFormatAangekomenDatum: computed ('aangekomenDatum', function(){
        return this.niceFormatDate(this.aangekomenDatum);
    }),

    niceFormatVerzondenDatum: computed ('verzondenDatum', function(){
        return this.niceFormatDate(this.verzondenDatum);
    }),

    niceFormatReactietermijn: computed ('reactietermijn', function(){

        if (this.reactietermijn == undefined)
            return "Geen reactie vreist";

        return "In bijna " + this.daysBetween(Date.parse(this.reactietermijn), new Date()) + " dagen";
    }),

    niceFormatDate: function (date) {
        
        if (date == undefined)
            return 'undefined';
        
        let parts = (date + "").split(" ");
        return [parts[2], parts[1], parts[3]].join(" ") + ", " + parts[4];
        
    },

    toDays: function (d) {
        d = d || 0;
        return d / 24 / 60 / 60 / 1000;
    },

    toUTC: function (d) { 
        if(!d || !d.getFullYear)return 0; 
        return Date.UTC(d.getFullYear(), d.getMonth(),d.getDate());
    },

    daysBetween: function (d1,d2){ 
        return this.toDays(this.toUTC(d2)-this.toUTC(d1)); 
    },

    failSafeTitle: computed ('title', function(){
        return this.title == "undefined" ? "Geen title" : (this.title + " title");
    })

});
