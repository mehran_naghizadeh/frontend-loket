import Model from 'ember-data/model';

export default Model.extend({
    id: 0,
    betreft: 'Meerjarenplanwijziging 2014-2018',
    typeCommunicatie: 'Opvraging in kader van een toezichtsdossier',
    dossierNummer: '2018.00301',
    reactieTermijn: 'Binner 30 dagen',
    laatsteBericht: '28-03-2018',
    laatsteVerstuurdDoor: 'Agentschap Binnenlands Bestuur',
});