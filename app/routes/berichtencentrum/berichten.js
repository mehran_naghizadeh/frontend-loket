import Route from '@ember/routing/route';
import DataTableRouteMixin from 'ember-data-table/mixins/route';

export default Route.extend(DataTableRouteMixin, {
    
    modelName: 'bericht',
    
    async model () {
        /*
        for (let i = 1; i < 6; ++i)
            await this.store.createRecord('bericht', {
                typeCommunicatie        : `type ${i}`,
                dossiernummer           : `nummer ${i}`,
                reactietermijn          : Date.parse(`2018-12-${i}T12:00:00-06:30`),
                laatsteBericht          : `laaste bericht ${i}`,
                laatstVerstuurdDoor     : `laatste versuurd door ${i}`,
                messageBody             : `A dummy message content regarding berich ${i}`,
                aangekomenDatum         : Date.parse(`2018-10-${i%30}T12:00:00-06:30`),
                verzondenDatum          : Date.parse(`2018-10-${i%30}T12:00:00-06:30`),
                title                   : `Bericht ${i}`
            }).save();
        */

        let data = this.store.findAll('bericht');
        return data;
    }
    
});

