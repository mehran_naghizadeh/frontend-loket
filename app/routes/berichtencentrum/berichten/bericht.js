import Route from '@ember/routing/route';

export default Route.extend({

    model(params) {
        let record = this.store.peekRecord ('bericht', params.id);
        return record;
    }
});
